#include "Niveau.h"


///Constructeur de niveau
Niveau::Niveau(int lon, int larg, std::string motpasse, bool affit=true) : m_long(lon+2), m_larg(larg+2), m_mdp(motpasse), m_affit(affit)
{
    m_matrice.resize(m_long);
    for(int i=0; i<m_long; i++)
        m_matrice[i].resize(m_larg);
}

///Destructeur
Niveau::~Niveau()
{

}

int Niveau::getm_long()const
{
    return m_long;
}

int Niveau::getm_larg() const
{
    return m_larg;
}

char Niveau::getLettre(int i, int j) const
{
    return m_matrice[i][j];
}

bool Niveau::getm_affit() const
{
    return m_affit;
}

void Niveau::nettoyerVect()///remplacer toutes les cases du vector par 0
{
    std::vector <int>::iterator it;///utilisation d'un it�rateur pour parcourir le vecteur
    for(it=m_posn.begin(); it!=m_posn.end(); it++)
    {
        (*it)=0;
    }
    for(it=m_posc.begin(); it!=m_posc.end(); it++)
    {
        (*it)=0;
    }
    for(it=m_post.begin(); it!=m_post.end(); it++)
    {
        (*it)=0;
    }
    for(it=m_posp.begin(); it!=m_posp.end(); it++)
    {
        (*it)=0;
    }
}

///Initialisation de la position des oiseaux
void Niveau::setPosOiseau( int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
{
    if (x1>=1 && y1>=1 && x1<=m_long-1 && y1<=m_larg-1)///Blindage sur les positions
    {
        m_oiseau[0]=x1;
        m_oiseau[1]=y1;
    }
    else
    {
        m_oiseau[0]=0;
        m_oiseau[1]=0;
    }
    if( x2>=1 && y2>=1 &&  x2<=m_long-1 && y2<=m_larg-1)///Blindage sur les positions
    {
        m_oiseau[2]=x2;
        m_oiseau[3]=y2;
    }
    else
    {
        m_oiseau[2]=0;
        m_oiseau[3]=0;
    }
    if(x3>=1 && y3>=1 &&  x3<=m_long-1 && y3<=m_larg-1 )///Blindage sur les positions
    {
        m_oiseau[4]=x3;
        m_oiseau[5]=y3;
    }
    else
    {
        m_oiseau[4]=0;
        m_oiseau[5]=0;
    }
    if(x4>=1 && y4>=1 && x4<=m_long-1&& y4<=m_larg-1)///Blindage sur les positions
    {
        m_oiseau[6]=x4;
        m_oiseau[7]=y4;
    }
    else
    {
        m_oiseau[6]=0;
        m_oiseau[7]=0;
    }
}

///Initialisation de la position des blocs n
void Niveau::setPosblocn( int x, int y)
{
    if (x>=1 && y>=1 && x<=m_long-1 && y<=m_larg-1)
    {
        m_posn.push_back(x);
        m_posn.push_back(y);
    }
}

///Initialisation de la position des blocs p
void Niveau::setPosblocp( int x, int y)
{
    if (x>=1 && y>=1 && x<=m_long-1 && y<=m_larg-1)
    {
        m_posp.push_back(x);
        m_posp.push_back(y);
    }
}

///Initialisation de la position des blocs c
void Niveau::setPosblocc( int x, int y)
{
    if (x>=1 && y>=1 && x<=m_long-1 && y<=m_larg-1)
    {
        m_posc.push_back(x);
        m_posc.push_back(y);
    }
}

///Initialisation de la position des blocs t
void Niveau::setPosbloct( int x, int y)
{
    if (x>=1 && y>=1 && x<=m_long-1 && y<=m_larg-1)
    {
        m_post.push_back(x);
        m_post.push_back(y);
    }
}

///Changement du contenu d'une case du vecteur
void Niveau::setCaseMat(int x, int y, char lettre)
{
    if(x>=0 && x<getm_long() && y>=0 && y<getm_larg()) m_matrice[x][y] = lettre;
}

std::vector< std::vector<char> >& Niveau::getm_matrice()
{
    return m_matrice;
}

///Cr�ation de la matrice � partir des donn�es stock�es
void Niveau::creermatrice()
{
    int i=0;

    for (i=0; i<m_long; i++)///Compl�ter par des espaces
        for ( int j=0; j<m_larg; j++)
            m_matrice[i][j]=' ';

    for (i=0; i<m_long; i++)///Contour
    {
        m_matrice[i][0]='$';
        m_matrice[i][m_larg-1]='$';
    }
    for (i=0; i<m_larg; i++)///Contour
    {
        m_matrice[0][i]='$';
        m_matrice[m_long-1][i]='$';
    }
    if (m_oiseau[0]>=1 && m_oiseau[1]>=1 && m_oiseau[0]<=m_long-1 && m_oiseau[1]<=m_larg-1) m_matrice[m_oiseau[0]][m_oiseau[1]]='O';///Mise en place des oiseaux
    if (m_oiseau[2]>=1 && m_oiseau[3]>=1 && m_oiseau[2]<=m_long-1 && m_oiseau[3]<=m_larg-1) m_matrice[m_oiseau[2]][m_oiseau[3]]='O';
    if (m_oiseau[4]>=1 && m_oiseau[5]>=1 && m_oiseau[4]<=m_long-1 && m_oiseau[5]<=m_larg-1) m_matrice[m_oiseau[4]][m_oiseau[5]]='O';
    if (m_oiseau[6]>=1 && m_oiseau[7]>=1 && m_oiseau[6]<=m_long-1 && m_oiseau[7]<=m_larg-1) m_matrice[m_oiseau[6]][m_oiseau[7]]='O';
    unsigned int j;
    if (m_posp.size()!=0)
    {
        j=0;
        while( j <= (m_posp.size()-1))
        {
            m_matrice[m_posp[j]][m_posp[j+1]]='P';///Placer les p
            j=j+2;
        }
    }
    if (m_posn.size()!=0)
    {
        j=0;
        while( j <= m_posn.size()-1)
        {
            m_matrice[m_posn[j]][m_posn[j+1]]='N';///Placer les n
            j=j+2;
        }
    }
    if (m_posc.size()!=0)
    {
        j=0;
        while( j <= m_posc.size()-1)
        {
            m_matrice[m_posc[j]][m_posc[j+1]]='C';///Placer les c
            j=j+2;
        }
    }
    if (m_post.size()!=0)
    {
        j=0;
        while( j <= m_post.size()-1)
        {
            m_matrice[m_post[j]][m_post[j+1]]='T';///Placer les t
            j=j+2;
        }
    }
}

void Niveau::setm_affit(bool affic)
{
    m_affit=affic;
}

///Affichage de la matrice
void Niveau::affichermat(Console* pConsole, bool edit, bool colo, char option, int stat)
{
    ///Affichage du quadrillage
    for ( int j=0; j<m_larg-1; j++)
    {
        if (j==0)
        {
            if (option=='1')
                pConsole->gotoLigCol(20,50);
            else if(option=='2')
                pConsole->gotoLigCol(20,30);
            else if (option=='3')
                pConsole->gotoLigCol(20,m_long+10);
            for ( int i=0; i<m_long-2; i++)
                std::cout<<"|+++";
            std::cout<<"|";
            if(option=='1')
                pConsole->gotoLigCol(21,50);
            else if (option=='2')
                pConsole->gotoLigCol(21,30);
            else if (option=='3')
                pConsole->gotoLigCol(21,m_long+10);
            for ( int i=0; i<m_long-2; i++)
                std::cout<<"|   ";
            std::cout<<"|";
        }
        else if (j!=m_larg-2)
        {
            if (option=='1')
                pConsole->gotoLigCol(20+2*j,50);
            else if (option=='2')
                pConsole->gotoLigCol(20+2*j,30);
            else if(option=='3')
                pConsole->gotoLigCol(20+2*j,m_long+10);
            for ( int i=0; i<m_long-2; i++)
                std:: cout<<"|---";
            std::cout<<"|";
            std::cout<<std::endl;
            if (option=='1')
                pConsole->gotoLigCol(21+2*j,50);
            else if (option=='2')
                pConsole->gotoLigCol(21+2*j,30);
            else if (option=='3')
                pConsole->gotoLigCol(21+2*j,m_long+10);
            for ( int i=0; i<m_long-2; i++)
                std::cout<<"|   ";
            std::cout<<"|";
            std::cout<<std::endl;
        }
        else
        {
            if (option=='1')
                pConsole->gotoLigCol(20+2*(m_larg-2),50);
            if (option=='2')
                pConsole->gotoLigCol(20+2*(m_larg-2),30);
            if (option=='3')
                pConsole->gotoLigCol(20+2*(m_larg-2),m_long+10);
            for ( int i=0; i<m_long-2; i++)
                std::cout<<"|+++";
            std::cout<<"|";
            std::cout<<std::endl;
        }
    }

    if(edit == false)
    {
        for( int i=1; i<m_long-1; i++)
            for( int j=1; j<m_larg-1; j++)

            {
                if (m_matrice[i][j]=='N')
                {
                    pConsole->gotoLigCol(19+2*j,48+4*i);
                    std::cout<<"N";
                }
                if (m_matrice[i][j]=='P')
                {
                    pConsole->gotoLigCol(19+2*j,48+4*i);
                    if (colo==true)
                        pConsole->setColor(COLOR_BLUE);
                    std::cout<<"P";
                    pConsole->setColor(COLOR_WHITE);
                }
                if (m_matrice[i][j]=='C')
                {
                    pConsole->gotoLigCol(19+2*j,48+4*i);
                    if (colo==true)
                        pConsole->setColor(COLOR_BLUE);
                    std::cout<<"C";
                    pConsole->setColor(COLOR_WHITE);
                }
                if (m_affit==true && m_matrice[i][j]=='T')
                {
                    pConsole->gotoLigCol(19+2*j,48+4*i);
                    if (colo==true)
                        pConsole->setColor(COLOR_RED);
                    std::cout<<"T";
                    pConsole->setColor(COLOR_WHITE);
                }
                if (m_matrice[i][j]=='O')
                {
                    pConsole->gotoLigCol(19+2*j,48+4*i);
                    if (colo==true)
                        pConsole->setColor(COLOR_YELLOW);
                    std::cout<<"O";
                    pConsole->setColor(COLOR_WHITE);
                }
            }
        pConsole->gotoLigCol(18,80);
        std::cout<<"Oiseaux manges :";///Affichage des statistiques
        if (stat==0)
        {
            pConsole->gotoLigCol(30,140);
            std::cout<<"Niveau :";
            pConsole->gotoLigCol(32,140);
            std::cout<<"Score :";
            pConsole->gotoLigCol(34,140);
            std::cout<<"Vie :";
        }
    }

}

///Cr�ation du niveau
void Niveau::creerNiveau(int niveau)
{
    int i, j;
    int x1=0, x2=0, x3=0, x4=0, y1=0, y2=0, y3=0, y4=0;
    char temp;
    ///Lecture dans un fichier pour avoir les donn�es
    // cr�er un flux de sortie
    std::ostringstream oss;
    // �crire un nombre dans le flux
    oss << "niveau" << niveau;
    // r�cup�rer une cha�ne de caract�res
    std::string nom = oss.str();
    nom+=".txt";
    ///std::cout << nom;
    std::ifstream fic(nom.c_str(), std::ios::in);
    if(fic)
    {
        nettoyerVect();
        for(j=0; j<10; j++)
        {
            for(i=0; i<20; i++)
            {
                fic.get(temp);
                if(temp=='N') setPosblocn(i+1,j+1);
                if(temp=='P') setPosblocp(i+1,j+1);
                if(temp=='T') setPosbloct(i+1,j+1);
                if(temp=='C') setPosblocc(i+1,j+1);
                if(temp=='O')
                {
                    if(x1==0 && y1==0)
                    {
                        x1=i+1;
                        y1=j+1;
                    }
                    else if(x2==0 && y2==0)
                    {
                        x2=i+1;
                        y2=j+1;
                    }
                    else if(x3==0 && y3==0)
                    {
                        x3=i+1;
                        y3=j+1;
                    }
                    else if(x4==0 && y4==0)
                    {
                        x4=i+1;
                        y4=j+1;
                    }
                }
            }
            fic.get(temp);
        }

        setPosOiseau(x1, y1, x2, y2, x3, y3, x4, y4);

        fic.close();
    }
    else // en cas d'erreur...
    {
        std::cout << "Cannot read niveau" << niveau << ".txt";
    }

}

///Editeur de niveau
bool Niveau::editeur(Console* pConsole, int& posx, int& posy, std::string& nom)
{
    char choix = 0;
    int ois=0;
    bool jouer = false, forcer=false, quit=false;
    int courx = 1, coury = 1; /// x et y courants utilis�s pour se d�placer dans le sp
    ///Initialisation de matrice vide
    int i=0;

    for (i=0; i<m_long; i++)
        for ( int j=0; j<m_larg; j++)
            setCaseMat(i, j, ' ');

    for (i=0; i<m_long; i++)
    {
        setCaseMat(i, 0, '$');
        setCaseMat(i, m_larg-1, '$');
    }
    for (i=0; i<m_larg; i++)
    {
        setCaseMat(0, i, '$');
        setCaseMat(m_long-1, i, '$');
    }
    affichermat(pConsole, true,true,'1');///Affichage d'une matrice vide
    pConsole->gotoLigCol(14, 50);
    std::cout << "Appuyez sur Echap pour quitter l'editeur";
    pConsole->gotoLigCol(16, 50);
    std::cout << "Appuyez sur x pour placer Snoopy";
    pConsole->gotoLigCol(18, 50);
    std::cout << "Nombre d'oiseaux a placer : ";
    pConsole->gotoLigCol(18, 80);
    std::cout << 4-ois;
    pConsole->gotoLigCol(19+2*(coury),48+4*(courx));

///Navigation et placement des blocs, oiseaux, personnage
    while(quit!=true)
    {
        choix = pConsole->getInputKey();
        switch(choix)
        {
        case 'z':
        case 'Z':
            if(coury > 1 && coury < getm_larg())
            {
                coury--;
                pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            }

            break;

        case 's':
        case'S':
            if(coury >= 1 && coury < getm_larg()-2)
            {
                coury++;
                pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            }
            break;

        case 'q':
        case'Q':
            if(courx > 1 && courx < getm_long())
            {
                courx--;
                pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            }
            break;

        case 'd':
        case'D':
            if(courx >= 1 && courx < getm_long()-2)
            {
                courx++;
                pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            }
            break;

        case 'o':
        case'O':
            if(ois<4)
            {
                setCaseMat(courx, coury, 'O');
                std::cout << "O";
                ois++;
                pConsole->gotoLigCol(18, 80);
                std::cout << 4-ois;
                pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            }

            break;

        case 'p':
        case'P':
            setCaseMat(courx, coury, 'P');
            std::cout << "P";
            pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            break;

        case 'n':
        case'N':
            setCaseMat(courx, coury, 'N');
            std::cout << "N";
            pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            break;

        case 't':
        case'T':
            setCaseMat(courx, coury, 'T');
            std::cout << "T";
            pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            break;

        case 'c':
        case'C':
            setCaseMat(courx, coury, 'C');
            std::cout << "C";
            pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            break;

        case 'x':
        case 'X':
            posx = courx;
            posy = coury;
            std::cout << "S";
            pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            break;

        case ' ':
            if(getLettre(courx, coury) == 'O')
            {
                ois--;
                pConsole->gotoLigCol(18, 80);
                std::cout << 4-ois;
            }
            setCaseMat(courx, coury, ' ');
            std::cout << " ";
            pConsole->gotoLigCol(19+2*(coury),48+4*(courx));
            break;
        case 27:
            if(ois<4)
            {
                pConsole->gotoLigCol(16, 90);
                std::cout << "Vous n'avez pas place assez d'oiseaux pour enregistrer.";///Blindage en cas d'oiseaux insuffisants
                pConsole->gotoLigCol(18, 90);
                std::cout << "Souhaitez-vous quitter l'editeur sans enregistrer ? o/n ";///Demande de confirmation
                choix = pConsole->getInputKey();
                if(choix == 'O' || choix=='o')
                {
                    forcer = true;
                    quit = true;
                    ois = 4;
                    pConsole->clean();
                }
                else
                {
                    pConsole->gotoLigCol(16, 90);
                    std::cout <<"                                                       ";
                    pConsole->gotoLigCol(18, 90);
                    std::cout <<"                                                       ";
                }
            }
            else
                quit = true;
            break;
        }
    }
    if(forcer == false)
    {
        pConsole->clean();
        pConsole->gotoLigCol(42, 50);
        std::cout << "Souhaitez-vous sauvegarder votre niveau ? o/n ";///Demande de sauvegarde du niveau
        choix = pConsole->getInputKey();
        if(choix == 'O' || choix=='o')
        {
            pConsole->clean();
            pConsole->gotoLigCol(20, 80);
            std::cout << "Entrez votre pseudo : ";
            std::cin >> (nom);
            (nom)+= ".txt";
            std::ofstream fic((nom).c_str(), std::ios::out | std::ios::trunc);
            if(fic) // si l'ouverture a r�ussi
            {
                for(int k=1; k<m_larg-1; k++)
                {
                    for(int l=1; l<m_long-1; l++)
                    {
                        if(getLettre(l, k)!='S' && getLettre(l, k)!='B') fic << getLettre(l, k);
                        else fic << " ";
                    }
                    fic << std::endl;
                }
                fic << "3x0x60x8x" << (posx) << "x" << (posy);
                fic.close();
            }
            else // en cas d'erreur...
            {
                std::cerr << "ERREUR SAUVEGARDE EDITEUR";///en cas d'erreur
            }
            pConsole->clean();
            pConsole->gotoLigCol(20, 80);
            std::cout << "Souhaitez-vous jouer dans votre niveau ? o/n ";///Jouer au niveau cr�� ?
            choix = pConsole->getInputKey();
            if(choix == 'o' || choix=='O') jouer = true;
        }
    }
    return jouer;
}


