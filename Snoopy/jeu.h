#ifndef JEU_H_INCLUDED
#define JEU_H_INCLUDED

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <ctime>
#include "console.h"
#include "Niveau.h"
#include <windows.h>


class Jeu
{
private:
    /// Attributs
    int m_score;
    int m_vie;
    clock_t m_temps;
    int m_nboiseau;
    int m_niveau;

public:
    /// Contructeurs et destructeurs
    Jeu();
    Jeu(int _score, int _vie, int _temps, int _nboiseau, int _niveau);
    ~Jeu();

    void setScore(bool etat, int score, int temps);
    void setVie(int indic);
    void setNiveau(int niv);
    int getm_score();
    int getm_vie();
    int getm_nboiseau();
    int getm_niveau();
    clock_t getm_temps();
    void changem_temps(clock_t t);
    void setm_nboiseau(int a);


    /// Methodes
    bool testvie(bool baisse);
    void affichertemps() const;///Affiche temps restant
    bool testertemps(int t);///Test la limite de temps
    void sauvegarde(Niveau* niv, int x, int y, Console* pConsole);///sauvegarde
    bool chargement(Niveau* niv, int* x, int* y, Console* pConsole, std::string nom);///charge
    void saveScore(Console* pConsole);///sauvegarde score
    void afficherTabScore(Console* pConsole);///Affiche leaderboard
};

#endif // JEU_H_INCLUDED
