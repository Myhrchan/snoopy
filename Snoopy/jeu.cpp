#include "jeu.h"
#include "niveau.h"


/// Constructeurs de Jeu
Jeu::Jeu()
    : m_score(0), m_vie(3), m_temps(60), m_nboiseau(0), m_niveau(1)
{
}

Jeu::Jeu(int _score, int _vie, int _temps, int _nboiseau, int _niveau)
    :m_score(_score), m_vie(_vie), m_temps(_temps), m_nboiseau(_nboiseau), m_niveau(_niveau)
{
}

/// Destructeur de Jeu
Jeu::~Jeu()
{
}

void Jeu::setScore(bool etat, int score, int temps)
{
    /// Si bool est true, le m_score prend la valeur envoy�e en param�tre (SEULEMENT EN CHARGEMENT DE PARTIE DEJA COMMENCEE !)
    /// Si bool est false, le score est calcul� en fonction du temps restant envoy�
    if(etat==true)  m_score = score;
    if(etat==false) m_score += 100 * temps;
}

void Jeu::setVie(int indic)
{
    if(m_vie > 0 && indic == 1) m_vie -= indic;
    else if(indic > 1) m_vie = indic;
}

void Jeu::changem_temps(clock_t t)
{
    m_temps=t;
}

clock_t Jeu::getm_temps()
{
    return m_temps;
}

/*void Jeu::settemps(int t)
{
    if (m_temps-t >=0)
    {
        m_temps-=t;
    }
    else m_temps = 0*CLOCKS_PER_SEC;
}*/


void Jeu::setNiveau(int niv)
{
    if(niv>0) m_niveau = niv;
}

void Jeu::setm_nboiseau(int a)
{
    if (a!=0)
        m_nboiseau+=a;
    else m_nboiseau=0;
}

int Jeu::getm_score()
{
    return m_score;
}
int Jeu::getm_vie()
{
    return m_vie;
}
int Jeu::getm_nboiseau()
{
    return m_nboiseau;
}
int Jeu::getm_niveau()
{
    return m_niveau;
}

/// Retire une vie si on lui envoie un bool�en true, et retourne l'�tat du joueur : true le joueur est mort, false il lui reste des vies
bool Jeu::testvie(bool baisse)
{
    bool deces;
    if (baisse == true) setVie(-1);
    if (m_vie == 0) deces = true;
    if (m_vie > 0) deces = false;
    return deces;
}

/// Teste si le temps est � 0, et renvoie un bool�en vrai si c'est le cas, sinon un bool�en false
bool Jeu::testertemps(int t)
{
    bool deces=false;
    if((t - (m_temps)/CLOCKS_PER_SEC) >= 60) deces = true;
    return deces;
}

/// Sauvegarde des donn�es dans un fichier txt
void Jeu::sauvegarde(Niveau* niv, int x, int y, Console* pConsole)
{
    std::string nom;
    pConsole->clean();
    pConsole->gotoLigCol(20, 80);
    std::cout << "Entrez votre pseudo : ";
    std::cin >> nom;
    nom += ".txt";

    std::ofstream fic(nom.c_str(), std::ios::out | std::ios::trunc);
    if(fic) // si l'ouverture a r�ussi
    {
        for(int j=1; j<11; j++)
        {
            for(int i=1; i<21; i++)
            {
                if(niv->getLettre(i, j)!='S' && niv->getLettre(i, j)!='B') fic << niv->getLettre(i, j);
                else fic << " ";
            }
            fic << std::endl;
        }
        fic << getm_vie () << "x";
        fic << getm_score() << "x";
        fic << m_temps << "x";
        fic << getm_niveau() << "x";
        fic << x << "x";
        fic << y;
        fic.close();
    }
    else // en cas d'erreur...
    {
        std::cout << "ERREUR ";
    }
}

/// Chargement de la matrice sauvegard�e
bool Jeu::chargement(Niveau* niv, int* x, int* y, Console* pConsole, std::string nom="")
{
    int i, j, posx, posy, x1=0, y1=0, x2=0, y2=0, x3=0, y3=0, x4=0, y4=0;
    char temp;
    bool charg = false;
    int etat=0;

    pConsole->clean();
    pConsole->gotoLigCol(20, 80);
    if(nom=="")
    {
        std::cout << "Entrez votre pseudo : ";
        std::cin >> nom;
        nom += ".txt";
    }

    std::ifstream fic(nom.c_str(), std::ios::in);
    if(fic)
    {
        charg= true;
        for(j=0; j<(niv->getm_larg()-2); j++)
        {
            for(i=0; i<(niv->getm_long()-2); i++)
            {
                fic.get(temp);
                ///Mise en place position blocs et oiseaux
                if(temp=='N') niv->setPosblocn(i+1, j+1);
                if(temp=='C') niv->setPosblocc(i+1, j+1);
                if(temp=='P') niv->setPosblocp(i+1, j+1);
                if(temp=='T') niv->setPosbloct(i+1, j+1);
                if(temp=='O')
                {
                    if(x1==0 && y1==0)
                    {
                        x1=i+1;
                        y1=j+1;
                    }
                    else if(x2==0 && y2==0)
                    {
                        x2=i+1;
                        y2=j+1;
                    }
                    else if(x3==0 && y3==0)
                    {
                        x3=i+1;
                        y3=j+1;
                    }
                    else if(x4==0 && y4==0)
                    {
                        x4=i+1;
                        y4=j+1;
                    }
                }
            }
            fic.get(temp);
        }
        Jeu::setm_nboiseau(0);
        if(x1==0 && y1==0) setm_nboiseau(1);
        if(x2==0 && y2==0) setm_nboiseau(1);
        if(x3==0 && y3==0) setm_nboiseau(1);
        if(x4==0 && y4==0) setm_nboiseau(1);

        niv->setPosOiseau(x1, y1, x2, y2, x3, y3, x4, y4);

        fic.get(temp);
        Jeu::setVie(temp - 48);
        fic.get(temp); // "x"
        fic.get(temp);
        etat = temp - 48;
        fic.get(temp);
        while(temp!='x')
        {
            etat = etat*10 + temp - 48;
            fic.get(temp);

        }
        Jeu::setScore(true, etat,0);
        etat=0;

        fic.get(temp);
        etat = temp - 48;
        fic.get(temp);
        while(temp!='x')
        {
            etat = etat*10 + temp - 48;
            fic.get(temp);
        }
        Jeu::changem_temps(etat);
        etat=0;

        fic.get(temp);
        Jeu::setNiveau(temp - 48);
        fic.get(temp);

        fic.get(temp);
        etat = temp - 48;
        fic.get(temp);
        while(temp!='x')
        {
            etat = etat*10 + temp - 48;
            fic.get(temp);
        }
        posx= etat;
        etat=0;
        fic.get(temp);
        etat = temp - 48;
        fic.get(temp);
        while(fic.eof() != true)
        {
            etat = etat*10 + temp - 48;
            fic.get(temp);
        }
        posy= etat;
        etat=0;

        fic.close();

        (*x)=posx;
        (*y)=posy;

    }
    else // en cas d'erreur...
    {
        pConsole->clean();
        pConsole->gotoLigCol(20, 80);
        std::cout << "PSEUDO INCORRECT";
        pConsole->pause(1000);
        pConsole->clean();
    }
    return charg;

}

void Jeu::saveScore(Console* pConsole)
{
    std::string nom;

    pConsole->clean();
    pConsole->gotoLigCol(20, 80);
    std::cout << "Entrez votre pseudo : ";
    std::cin >> nom;
    std::ofstream fic("scores.txt", std::ios::out | std::ios::app);///Sauvegarde dans scores.txt
    if(fic)
    {
        fic << nom << " " << getm_score() << std::endl;///sauvegarde du score
        fic.close();
    }
    else ///Cas d'erreur
    {
        pConsole->clean();
        pConsole->gotoLigCol(20, 80);
        std::cerr << "PROBLEME OUVERTURE DU FICHIER SCORES POUR ENREGISTREMENT";
    }
}

void Jeu::afficherTabScore(Console* pConsole)///Affichage du leaderboard
{
    std::ifstream fic("scores.txt", std::ios::in);
    std::string nom;
    char temp = 'a';
    int score, i=2;

    pConsole->clean();
    pConsole->gotoLigCol(15, 80);
    std::cout << "            MEILLEURS SCORES";
    pConsole->gotoLigCol(20, 80);
    std::cout << "-----------------------------------------";

    if(fic)
    {
        while(fic.eof() != true)
        {
            fic >> nom; // obtention du nom
            //   fic.get(temp); // " "

            fic >> score;    //score = score*10 + temp - 48;
            fic.get(temp);

            pConsole->gotoLigCol(20+i, 80);
            std::cout <<"| " << nom;
            pConsole->gotoLigCol(20+i-1, 80);
            std::cout <<"| ";
            pConsole->gotoLigCol(20+i, 100);
            std::cout <<"| " << score;
            pConsole->gotoLigCol(20+i-1, 100);
            std::cout <<"| ";
            pConsole->gotoLigCol(20+i, 120);
            std::cout <<"|";
            pConsole->gotoLigCol(20+i-1, 120);
            std::cout <<"|";
            i=i+2;
            pConsole->gotoLigCol(20+i-1, 80);
        }
        std::cout << "-----------------------------------------";
        fic.close();
    }


    else
    {
        pConsole->clean();
        pConsole->gotoLigCol(20, 90);
        std::cout << "AUCUN SCORE ENREGISTRE";
    }


}


