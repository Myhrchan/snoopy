#include "Puzzle.h"

void Puzzle::accueil(Console* cons)
{
    int lig, col;
    lig=4;
    col=78;
    /// Affichage S
    cons->setColor(COLOR_RED);
    cons->gotoLigCol(lig, col+1);
    std::cout << " _______";
    cons->gotoLigCol(lig+1, col);
    std::cout <<" /       |";
    cons->gotoLigCol(lig+2, col);
    std::cout <<"|    ____|";
    cons->gotoLigCol(lig+3, col);
    std::cout <<"|   |";
    cons->gotoLigCol(lig+4, col);
    std::cout <<"|   `----";
    cons->gotoLigCol(lig+5, col);
    std::cout <<"|_____   |";
    cons->gotoLigCol(lig+6, col);
    std::cout <<"      |  |";
    cons->gotoLigCol(lig+7, col);
    std::cout <<" -----   |";
    cons->gotoLigCol(lig+8, col);
    std::cout <<"|        |";
    cons->gotoLigCol(lig+9, col);
    std::cout <<"|_______/";

    ///Affichage n
    lig = lig+4;
    col= col + 10;
    cons->gotoLigCol(lig, col);
    std::cout << " _  ___";
    cons->gotoLigCol(lig+1, col);
    std::cout << "| |/   |";
    cons->gotoLigCol(lig+2, col);
    std::cout << "|  __  |";
    cons->gotoLigCol(lig+3, col);
    std::cout << "| |  | |";
    cons->gotoLigCol(lig+4, col);
    std::cout << "| |  | |";
    cons->gotoLigCol(lig+5, col);
    std::cout << "|_|  |_|";

    ///Affichage o
    for(int i=0; i<2; i++)
    {
        col= col + 8;
        cons->gotoLigCol(lig, col);
        std::cout << "  _____";
        cons->gotoLigCol(lig+1, col);
        std::cout << " /     |";
        cons->gotoLigCol(lig+2, col);
        std::cout << "|  __  |";
        cons->gotoLigCol(lig+3, col);
        std::cout << "| |  | |";
        cons->gotoLigCol(lig+4, col);
        std::cout << "| |__| |";
        cons->gotoLigCol(lig+5, col);
        std::cout << "|_____/";
    }

    ///Affichage p
    col= col + 8;
    cons->gotoLigCol(lig, col);
    std::cout << "  _____";
    cons->gotoLigCol(lig+1, col);
    std::cout << " /     |";
    cons->gotoLigCol(lig+2, col);
    std::cout << "|  __  |";
    cons->gotoLigCol(lig+3, col);
    std::cout << "| |  | |";
    cons->gotoLigCol(lig+4, col);
    std::cout << "| |__| |";
    cons->gotoLigCol(lig+5, col);
    std::cout << "|   __/";
    cons->gotoLigCol(lig+6, col);
    std::cout << "|  |";
    cons->gotoLigCol(lig+7, col);
    std::cout << "|  |";
    cons->gotoLigCol(lig+8, col);
    std::cout << "|  |";
    cons->gotoLigCol(lig+9, col);
    std::cout << "|_/";

    ///Affichage p
    col= col + 8;
    cons->gotoLigCol(lig, col);
    std::cout << " _    _";
    cons->gotoLigCol(lig+1, col);
    std::cout << "| |  | |";
    cons->gotoLigCol(lig+2, col);
    std::cout << "| |  | |";
    cons->gotoLigCol(lig+3, col);
    std::cout << "| |  | |";
    cons->gotoLigCol(lig+4, col);
    std::cout << "| `--' |";
    cons->gotoLigCol(lig+5, col);
    std::cout << "`___   |";
    cons->gotoLigCol(lig+6, col);
    std::cout << "    |  |";
    cons->gotoLigCol(lig+7, col);
    std::cout << "    |  |";
    cons->gotoLigCol(lig+8, col);
    std::cout << "    |  |";
    cons->gotoLigCol(lig+9, col);
    std::cout << "    |_/";

    ///Afficher The Revenge
    lig = 14;
    col = 75;
    cons->setColor(COLOR_YELLOW);
    cons->gotoLigCol(lig, col);
    std::cout << "___     __   __ __     __     __  __";
    cons->gotoLigCol(lig+1, col);
    std::cout << " | |__||__  |_/|__\\  /|__|\\ ||  _|__";
    cons->gotoLigCol(lig+2, col);
    std::cout << " | |  ||__  | \\|__ \\/ |__| \\||__||__";
    cons->setColor(COLOR_WHITE);
}

void Puzzle::lancjeu()
{

    bool quit=true, affichage=true;
    int choix, x, y;
    std::string mdp;
    Niveau niv(20, 10, "a", affichage);
    Personnage Sp(10,5);
    Personnage& Snoop = Sp;
    bool ok;
    Console* pConsole;
    pConsole=Console::getInstance();
    pConsole->taille();
    pConsole->setColor(COLOR_WHITE);
    Puzzle::accueil(pConsole);
    pConsole->clean();
    do
    {
        choix =Puzzle::menuprinc(pConsole);
        pConsole->clean();
        switch (choix)
        {
        case 1 :///Jeu
            jeubase.setNiveau(1);
            Puzzle::gam(pConsole,affichage,&niv,Snoop, ok);
            if(jeubase.getm_niveau() == 7) jeubase.saveScore(pConsole);
            jeubase.setScore(true,0,0);
            break;
        case 2 :///Charger
            ok=jeubase.chargement(&niv, &x, &y, pConsole, "");
            if(ok == true)
            {
                Snoop.setabsoluteposx(x);
                Snoop.setabsoluteposy(y);
                Puzzle::gam(pConsole,affichage,&niv, Snoop, ok);
                if(jeubase.getm_niveau() == 7) jeubase.saveScore(pConsole);
                jeubase.setScore(true,0,0);
            }

            break;
        case 3:///Mot de passe
            pConsole->clean();
            pConsole->gotoLigCol(30,80);
            std::cout<<"Quel est votre mot de passe vers le pouvoir ? Q pour quitter";
            pConsole->gotoLigCol(32,80);
            do
            {
                std::getline(std::cin,mdp);
            }
            while (mdp.empty());
            if (mdp=="skipitskipit")
            {
                jeubase.setNiveau(2);
                Puzzle::gam(pConsole,affichage,&niv,Snoop, false);
                if(jeubase.getm_niveau() == 7) jeubase.saveScore(pConsole);
                jeubase.setScore(true,0,0);
            }
            else if (mdp=="snoopsnoopbandicoot")
            {
                jeubase.setNiveau(3);
                Puzzle::gam(pConsole,affichage,&niv,Snoop, false);
                if(jeubase.getm_niveau() == 7) jeubase.saveScore(pConsole);
                jeubase.setScore(true,0,0);
            }
            else if (mdp=="test")
            {
                jeubase.setNiveau(4);
                Puzzle::gam(pConsole,affichage,&niv,Snoop, false);
            }
            else if (mdp=="test2")
            {
                jeubase.setNiveau(5);
                Puzzle::gam(pConsole,affichage,&niv,Snoop, false);
            }
            else if (mdp=="test3")
            {
                jeubase.setNiveau(6);
                Puzzle::gam(pConsole,affichage,&niv,Snoop, false);
            }
            else if (mdp=="q"||mdp=="Q")
            {
                pConsole->clean();
            }
            else
            {
                pConsole->gotoLigCol(34,80);
                std::cout<<"Mot de passe non reconnu, l'acces vous est refuse !!";
                pConsole->pause(1500);
                pConsole->clean();
            }

            break;
        case 4:///Mode j'ai la foi

            Puzzle::jailafoi(pConsole, affichage);
            break;
        case 5 :///Options
            Puzzle::menuoption(pConsole, affichage, niv, Snoop);
            break;
        case 6 :///Score
            jeubase.afficherTabScore(pConsole);
            pConsole->gotoLigCol(18,90);
            std::cout<<"Appuyez sur une touche";
            pConsole->getInputKey();
            pConsole->clean();
            break;

        case 7:///Quitter
            pConsole->clean();
            pConsole->gotoLigCol(5,5);
            std::cout<<"Au revoir !!"<<std::endl;
            quit=false;
            pConsole->deleteInstance();
            break;
        default:
            std::cerr<<"Saisie invalide"<<std::endl;
            break;
        }
    }
    while(quit);
}

void Puzzle::gam(Console* pConsole,bool affichage, Niveau* niveau1, Personnage& Snoop, bool ok)
{
    bool quittergam=false;
    clock_t entmenu,sortimenu;
    int tempsmenu;
    do
    {
        if(ok==false)
            niveau1->creerNiveau(jeubase.getm_niveau());
        if(jeubase.getm_niveau()>20)
            jeubase.setNiveau(jeubase.getm_niveau()-20);
            if (ok==true)
                ok=false;
        tempsmenu=0;
        niveau1->creermatrice();
        niveau1->setm_affit(affichage);
        pConsole->clean();
        Puzzle::intro(pConsole);
        if (jeubase.getm_niveau()!=5 && jeubase.getm_niveau()<7)
        niveau1->affichermat(pConsole, false, true,'1');
        else if (jeubase.getm_niveau()<7)niveau1->affichermat(pConsole, false, false,'1');
        else if(jeubase.getm_niveau()>=7)niveau1->affichermat(pConsole, false, true,'1',1);
        if (jeubase.getm_niveau()<5)
        Puzzle::affitimer(pConsole, jeubase,1);
        if(ok==false)
        {
            if (jeubase.getm_niveau()!=5)
            {
                Snoop.setabsoluteposx(10);
                Snoop.setabsoluteposy(5);
            }
            else
            {
                Snoop.setabsoluteposx(20);
                Snoop.setabsoluteposy(10);
            }
        }
        pConsole->gotoLigCol(19+2*(Snoop.getm_posy()),48+4*(Snoop.getm_posx()));
        std::cout<<"S";
        Balle balle(12,4,1,1);
        Balle balle2(1,1,1,1);
        Balle balle3 (5,8,-1,-1);
        Balle balle4 (4,7,-1,1);
        Balle balle5 (9,2,1,-1);
        Balle balle6 (10,5,-1,-1);
        Balle balle7 (1,10,1,-1);
        Balle balle8 (18,2,-1,-1);
        pConsole->pause(500);
        jeubase.changem_temps(clock());
        bool recommencer=false;
        char touche;
        do
        {
            if (jeubase.getm_niveau()!=5)
            {
                balle.depballe(niveau1->getm_matrice(),pConsole, Snoop);
                balle.collision(niveau1->getm_matrice());
            }
            if(jeubase.getm_niveau()==3||jeubase.getm_niveau()==4||jeubase.getm_niveau()==6)
            {
                balle2.depballe(niveau1->getm_matrice(),pConsole, Snoop);
                balle2.collision(niveau1->getm_matrice());
            }
            if(jeubase.getm_niveau()==6)
            {
                balle3.depballe(niveau1->getm_matrice(),pConsole, Snoop);
                balle3.collision(niveau1->getm_matrice());
                balle4.depballe(niveau1->getm_matrice(),pConsole, Snoop);
                balle4.collision(niveau1->getm_matrice());
                balle5.depballe(niveau1->getm_matrice(),pConsole, Snoop);
                balle5.collision(niveau1->getm_matrice());
                balle6.depballe(niveau1->getm_matrice(),pConsole, Snoop);
                balle6.collision(niveau1->getm_matrice());
                balle7.depballe(niveau1->getm_matrice(),pConsole, Snoop);
                balle7.collision(niveau1->getm_matrice());
                balle8.depballe(niveau1->getm_matrice(),pConsole, Snoop);
                balle8.collision(niveau1->getm_matrice());
                pConsole->pause(50);
            }
            if (pConsole->isKeyboardPressed())
            {
                touche=pConsole->getInputKey();
                switch (touche)
                {
                case 27:
                    entmenu=clock();
                    Puzzle::menuechap(quittergam, pConsole, &jeubase, niveau1, Snoop, affichage, recommencer);
                    sortimenu=clock();
                    tempsmenu+=(sortimenu-entmenu)/CLOCKS_PER_SEC;
                    break;
                case 'P':
                case'p':
                    pConsole->getInputKey();
                    break;
                default:
                    Snoop.mouvement(touche, niveau1->getm_matrice(),pConsole, jeubase, tempsmenu);
                    break;
                }
            }
            if(jeubase.getm_nboiseau() == 4)
                jeubase.setNiveau(jeubase.getm_niveau()+1);
            pConsole->gotoLigCol(18,100);
            std::cout<<jeubase.getm_nboiseau();
            if (jeubase.getm_niveau()<7){
            pConsole->gotoLigCol(30,149);
            std::cout<<jeubase.getm_niveau();
            pConsole->gotoLigCol(32,149);
            std::cout<<jeubase.getm_score();
            pConsole->gotoLigCol(34,149);
            std::cout<<jeubase.getm_vie();}
            Puzzle::progtimer(jeubase.getm_temps(),pConsole,tempsmenu);
            if (Snoop.getm_mort()==false && jeubase.getm_niveau()<5)
                Snoop.setmort(jeubase.testertemps((clock()/CLOCKS_PER_SEC)-tempsmenu));
            pConsole->pause(100);

        }
        while (Snoop.getm_mort()==false && jeubase.getm_nboiseau()!=4 && quittergam==false&&recommencer==false);
        if (jeubase.getm_nboiseau()==4)
        {
            jeubase.setScore(false,jeubase.getm_score(),(clock()-jeubase.getm_temps()-tempsmenu)/CLOCKS_PER_SEC );
        }
        jeubase.setm_nboiseau(0);
        jeubase.changem_temps(clock());
        pConsole->clean();
        if (Snoop.getm_mort()==true)
        {
            jeubase.setVie(1);
            if (jeubase.getm_vie()!=0)
            {
                pConsole->gotoLigCol(30,100);
                std::cout<<"Vous etes mort !!";
                pConsole->gotoLigCol(32,98);
                std::cout<<"Il vous reste "<<jeubase.getm_vie()<<" vies.";
                Snoop.setmort(false);
                pConsole->pause(800);
            }
            else if (jeubase.getm_vie()==0)
            {
                pConsole->gotoLigCol(30,100);
                std::cout<<"Vous etes mort !!";
                pConsole->gotoLigCol(32,105);
                std::cout<<"GAME OVER";
                pConsole->pause(800);
            }
        }

    }
    while(quittergam==false && jeubase.getm_vie()>0 && jeubase.getm_niveau()<7);
    jeubase.setVie(3);
    pConsole->clean();
}

void Puzzle::menuoption(Console* pConsole,bool& affichage, Niveau& niv, Personnage& Snoop)
{
    int retour=1,i=30, x=10, y=5;
    char a;
    bool ok=false, edit;
    std::string pseudo;
    pConsole->clean();
    pConsole->gotoLigCol(15,100);
    std::cout<<"Options";
    pConsole->gotoLigCol(30,95);
    std::cout<<"Afficher Pieges";
    pConsole->gotoLigCol(32,100);
    std::cout<<"Bonus";
    pConsole->gotoLigCol(34,100);
    std::cout<<"Retour";
    pConsole->gotoLigCol(30,90);
    std::cout<<"<<";
    pConsole->gotoLigCol(30,115);
    std::cout<<">>";
    do
    {
        a=pConsole->getInputKey();
        switch (a)
        {
        case -32:
            void creerNiveau1();
            char b;
            b=pConsole->getInputKey();
            switch (b)
            {
            case 80:
                if (retour<3)
                {
                    pConsole->gotoLigCol(i,115);
                    std::cout<<"  ";
                    pConsole->gotoLigCol(i,90);
                    std::cout<<"  ";
                    i+=2;
                    pConsole->gotoLigCol(i,115);
                    std::cout<<">>";
                    pConsole->gotoLigCol(i,90);
                    std::cout<<"<<";
                    retour+=1;
                }
                break;
            case 72 :
                if (retour>1)
                {
                    pConsole->gotoLigCol(i,115);
                    std::cout<<"  ";
                    pConsole->gotoLigCol(i,90);
                    std::cout<<"  ";
                    i-=2;
                    pConsole->gotoLigCol(i,115);
                    std::cout<<">>";
                    pConsole->gotoLigCol(i,90);
                    std::cout<<"<<";
                    retour-=1;
                }
                break;
            default:
                break;
            }
            break;
        case 13 :
            ok=true;
            for(int u=90; u<=115; u+=2)
            {
                pConsole->gotoLigCol(i, u);
                std::cout<<"  >>";
                pConsole->gotoLigCol(i,113-(u-90));
                std::cout<<"<<  ";
                pConsole->pause(100);
            }
            break;
        default :
            break;
        }
    }
    while(!ok);
    if (retour==1)
    {
        int choix=1;
        pConsole->clean();
        pConsole->gotoLigCol(15,100);
        std::cout<<"Affichage des pieges";
        pConsole->gotoLigCol(20,50);
        std::cout<<"<< Afficher >>";
        pConsole->gotoLigCol(20,150);
        std::cout<<"   Ne pas afficher";
        ok=false;
        do
        {
            a=pConsole->getInputKey();
            switch (a)
            {
            case -32:
                char b;
                b=pConsole->getInputKey();
                switch (b)
                {
                case 77:
                    if (choix<2)
                    {
                        pConsole->gotoLigCol(20,62);
                        std::cout<<"  ";
                        pConsole->gotoLigCol(20,50);
                        std::cout<<"  ";
                        pConsole->gotoLigCol(20,169);
                        std::cout<<">>";
                        pConsole->gotoLigCol(20,150);
                        std::cout<<"<<";
                        choix+=1;
                    }
                    break;
                case 75 :
                    if (choix>1)
                    {
                        pConsole->gotoLigCol(20,169);
                        std::cout<<"  ";
                        pConsole->gotoLigCol(20,150);
                        std::cout<<"  ";
                        pConsole->gotoLigCol(20,62);
                        std::cout<<">>";
                        pConsole->gotoLigCol(20,50);
                        std::cout<<"<<";
                        choix-=1;
                    }
                    break;
                default:
                    break;
                }
                break;
            case 13 :
                ok=true;
                pConsole->clean();
                pConsole->gotoLigCol(30,100);
                if(choix==1)
                {
                    std::cout<<"L'affichage est actif";
                    affichage=true;
                }
                else
                {
                    std::cout<<"L'afficage n'est plus prise en compte";
                    affichage=false;
                }
                pConsole->pause(800);
                pConsole->clean();
                break;
            default :
                break;
            }
        }
        while(!ok);

    }
    if (retour==2)///BONUS
    {
        char d;
        int retour2=1, l=30;
        bool ok1=false;
        pConsole->clean();
        pConsole->gotoLigCol(30,95);
        std::cout<<"Editeur de niveau";
        pConsole->gotoLigCol(32,95);
        std::cout<<"Retour";
        pConsole->gotoLigCol(30,90);
        std::cout<<"<<";
        pConsole->gotoLigCol(30,115);
        std::cout<<">>";
        do
        {
            d=pConsole->getInputKey();
            switch (d)
            {
            case -32:
                void creerNiveau1();
                char c;
                c=pConsole->getInputKey();
                switch (c)
                {
                case 80:
                    if (retour2<2)
                    {
                        pConsole->gotoLigCol(l,115);
                        std::cout<<"  ";
                        pConsole->gotoLigCol(l,90);
                        std::cout<<"  ";
                        l+=2;
                        pConsole->gotoLigCol(l,115);
                        std::cout<<">>";
                        pConsole->gotoLigCol(l,90);
                        std::cout<<"<<";
                        retour2+=1;
                    }
                    break;
                case 72 :
                    if (retour2>1)
                    {
                        pConsole->gotoLigCol(l,115);
                        std::cout<<"  ";
                        pConsole->gotoLigCol(l,90);
                        std::cout<<"  ";
                        l-=2;
                        pConsole->gotoLigCol(l,115);
                        std::cout<<">>";
                        pConsole->gotoLigCol(l,90);
                        std::cout<<"<<";
                        retour2-=1;
                    }
                    break;
                default:
                    break;
                }
                break;
            case 13 :
                ok1=true;
                for(int u=90; u<=115; u+=2)
                {
                    pConsole->gotoLigCol(l, u);
                    std::cout<<"  >>";
                    pConsole->gotoLigCol(l,113-(u-90));
                    std::cout<<"<<  ";
                    pConsole->pause(100);
                }
                break;
            default :
                break;
            }
        }
        while(!ok1);
        switch(retour2)
        {
        case 1:/// Editeur de niveau
            edit=niv.editeur(pConsole, x, y, pseudo);
            if(edit == true)
            {
                Snoop.setabsoluteposx(x);
                Snoop.setabsoluteposy(y);
                ok=jeubase.chargement(&niv, &x, &y, pConsole, pseudo);
                if(ok == true)
                {
                    jeubase.setNiveau(8);
                    Puzzle::gam(pConsole,affichage,&niv, Snoop, ok);
                }
            }
            else accueil(pConsole);
            break;
        default:
            pConsole->clean();
            break;
        }
    }
    if (retour==3)
        pConsole->clean();
}

int Puzzle::menuprinc(Console* pConsole)
{
    char a;
    int i=30;
    bool ok=false;
    int retour=1;
    Puzzle::accueil(pConsole);
    pConsole->gotoLigCol(30,90);
    std::cout<<"Appuyez sur une touche";
    pConsole->getInputKey();
    pConsole->gotoLigCol(30,90);
    std::cout<<"                      ";
    pConsole->gotoLigCol(30,100);
    std::cout<<"Jeu";
    //charger
    pConsole->gotoLigCol(32,98);
    std::cout<<"Charger";
    //mdp
    pConsole->gotoLigCol(34,96);
    std::cout<<"Mot de passe";
    pConsole->gotoLigCol(36,94);
    std::cout<<"Mode j'ai la foi !";
    pConsole->gotoLigCol(38,98);
    std::cout<<"Options";
    //scores
    pConsole->gotoLigCol(40,98);
    std::cout<<"Scores";
    pConsole->gotoLigCol(42,98);
    std::cout<<"Quitter";
    pConsole->gotoLigCol(30,90);
    std::cout<<"<<";
    pConsole->gotoLigCol(30,115);
    std::cout<<">>";
    do
    {
        a=pConsole->getInputKey();
        switch (a)
        {
        case -32:
            char b;
            b=pConsole->getInputKey();
            switch (b)
            {
            case 80:
                if (retour<7)
                {
                    pConsole->gotoLigCol(i,115);
                    std::cout<<"  ";
                    pConsole->gotoLigCol(i,90);
                    std::cout<<"  ";
                    i+=2;
                    pConsole->gotoLigCol(i,115);
                    std::cout<<">>";
                    pConsole->gotoLigCol(i,90);
                    std::cout<<"<<";
                    retour+=1;
                }
                break;
            case 72 :
                if (retour>1)
                {
                    pConsole->gotoLigCol(i,115);
                    std::cout<<"  ";
                    pConsole->gotoLigCol(i,90);
                    std::cout<<"  ";
                    i-=2;
                    pConsole->gotoLigCol(i,115);
                    std::cout<<">>";
                    pConsole->gotoLigCol(i,90);
                    std::cout<<"<<";
                    retour-=1;
                }
                break;
            default:
                break;
            }
            break;
        case 13 :
            ok=true;
            for(int u=90; u<=115; u+=2)
            {
                pConsole->gotoLigCol(i, u);
                std::cout<<"  >>";
                pConsole->gotoLigCol(i,113-(u-90));
                std::cout<<"<<  ";
                pConsole->pause(100);
            }
            break;
        default :
            break;
        }
    }
    while(!ok);
    return retour;
}

void Puzzle::intro(Console* pConsole)
{
    int i=1,j=2;
    pConsole->gotoLigCol(0,0);
    std::cout<<"---";
    pConsole->gotoLigCol(1,0);
    std::cout<<"|";
    pConsole->gotoLigCol(2,0);
    std::cout<<"|";
    do
    {
        pConsole->gotoLigCol(i+1,j);
        std::cout<<"|";
        pConsole->gotoLigCol(i+2,j);
        std::cout<<"|";
        pConsole->gotoLigCol(i+3,j-2);
        std::cout<<"---";
        pConsole->pause(18);
        pConsole->gotoLigCol(i+1,j);
        std::cout<<" ";
        pConsole->gotoLigCol(i+2,j);
        std::cout<<" ";
        pConsole->gotoLigCol(i+3,j-2);
        std::cout<<"   ";
        i+=1;
        j+=3;
    }
    while(i<80 && j<200);
    pConsole->gotoLigCol(82,0);
    std::cout<<"---";
    pConsole->gotoLigCol(81,0);
    std::cout<<"|";
    pConsole->gotoLigCol(80,0);
    std::cout<<"|";
    pConsole->gotoLigCol(0,197);
    std::cout<<"---";
    pConsole->gotoLigCol(1,199);
    std::cout<<"|";
    pConsole->gotoLigCol(2,199);
    std::cout<<"|";
    pConsole->gotoLigCol(82,197);
    std::cout<<"---";
    pConsole->gotoLigCol(81,199);
    std::cout<<"|";
    pConsole->gotoLigCol(80,199);
    std::cout<<"|";
}

void Puzzle::affitimer(Console* pConsole,Jeu jeubase, int colo)
{
        pConsole->gotoLigCol(45,45);
        if (colo==1)
            pConsole->setColor(COLOR_BLUE);
        if (colo==2)
            pConsole->setColor(COLOR_RED);
        for(unsigned int i=0; i<90; i++)
        {
            std::cout<<"-";
        }
        pConsole->gotoLigCol(15,45);
        for(unsigned int i=0; i<90; i++)
        {
            std::cout<<"-";
        }
        for(unsigned int i=0; i<30; i++)
        {
            pConsole->gotoLigCol(15+i,45);
            std::cout<<"|";
        }
        for(unsigned int i=0; i<30; i++)
        {
            pConsole->gotoLigCol(15+i,135);
            std::cout<<"|";
        }
        pConsole->setColor(COLOR_WHITE);
}

void Puzzle::progtimer(clock_t timeini, Console* pConsole, int tempsmenu)
{
    int temps=((clock()-timeini)/CLOCKS_PER_SEC-tempsmenu);
    if (temps<=22)
    {
        pConsole->gotoLigCol(15,45);
        for(int i=0; i<4*temps; i++)
            std::cout<<" ";///Effacer haut
    }

    else if (temps==23)
    {
        pConsole->gotoLigCol(15,45);
        for(unsigned int i=0; i<90; i++)
            std::cout<<" ";///Effacer tot haut
        for(int i=0; i<3; i++)
        {
            pConsole->gotoLigCol(15+i,135);
            std::cout<<" ";///Effacer c�t�
        }
    }

    else if(temps<=30)
    {
        pConsole->gotoLigCol(15,45);
        for(unsigned int i=0; i<90; i++)
            std::cout<<" ";///Effacer tot haut
        for(int i=0; i<4*(temps-22); i++)
        {
            pConsole->gotoLigCol(15+i,135);
            std::cout<<" ";///Effacer c�t�
        }
    }

    else if(temps<=52)
    {
        pConsole->gotoLigCol(15,45);
        for(unsigned int i=0; i<90; i++)
            std::cout<<" ";///Effacer tot haut
        for(int i=0; i<30; i++)
        {
            pConsole->gotoLigCol(15+i,135);
            std::cout<<" ";///Effacer c�t�
        }
        for(int i=0; i<4*(temps-30); i++)
        {
            pConsole->gotoLigCol(45,135-i);
            std::cout<<" ";///Effacer bas
        }
    }

    else if(temps==53)
    {
        pConsole->gotoLigCol(15,45);
        for(unsigned int i=0; i<90; i++)
            std::cout<<" ";///Effacer tot haut
        for(int i=0; i<30; i++)
        {
            pConsole->gotoLigCol(15+i,135);
            std::cout<<" ";///Effacer c�t�
        }
        pConsole->gotoLigCol(45,45);
        for(unsigned int i=0; i<90; i++)
            std::cout<<" ";///Effacer bas
        for(unsigned int i=0; i<3; i++)
        {
            pConsole->gotoLigCol(45-i,45);
            std::cout<<" ";///Effacer c�t�
        }

    }

    else
    {
        pConsole->gotoLigCol(15,45);
        for(unsigned int i=0; i<90; i++)
            std::cout<<" ";///Effacer tot haut
        for(int i=0; i<30; i++)
        {
            pConsole->gotoLigCol(15+i,135);
            std::cout<<" ";///Effacer c�t�
        }
        pConsole->gotoLigCol(45,45);
        for(unsigned int i=0; i<90; i++)
            std::cout<<" ";///Effacer bas
        for(int i=0; i<4*(temps-53); i++)
        {
            pConsole->gotoLigCol(42-i,45);
            std::cout<<" ";///Effacer c�t�
        }
    }

}

void Puzzle::menuechap (bool& quittergam, Console* pConsole, Jeu* jeubase, Niveau* niveau, Personnage& Snoop, bool& affichage, bool& recommencer)
{
    int retour=1;
    int i=30;
    bool ok=false,save=false, quit=false;
    do
    {
        pConsole ->clean();
        Puzzle::accueil(pConsole);
        pConsole->gotoLigCol(30, 100);
        std::cout<<"Reprendre";
        pConsole->gotoLigCol(32, 98);
        std::cout<<"Sauvegarder";
        pConsole->gotoLigCol(34,98);
        std::cout<<"Recommencer";
        pConsole->gotoLigCol(36, 101);
        std::cout<<"Options";
        pConsole->gotoLigCol(38, 101);
        std::cout<<"Quitter";
        ok=false;
        do
        {
            char a=pConsole->getInputKey();
            switch (a)
            {
            case -32:
                char b;
                b=pConsole->getInputKey();
                switch (b)
                {
                case 80:
                    if (retour<5)
                    {
                        pConsole->gotoLigCol(i,115);
                        std::cout<<"  ";
                        pConsole->gotoLigCol(i,90);
                        std::cout<<"  ";
                        i+=2;
                        pConsole->gotoLigCol(i,115);
                        std::cout<<">>";
                        pConsole->gotoLigCol(i,90);
                        std::cout<<"<<";
                        retour+=1;
                    }
                    break;
                case 72 :
                    if (retour>1)
                    {
                        pConsole->gotoLigCol(i,115);
                        std::cout<<"  ";
                        pConsole->gotoLigCol(i,90);
                        std::cout<<"  ";
                        i-=2;
                        pConsole->gotoLigCol(i,115);
                        std::cout<<">>";
                        pConsole->gotoLigCol(i,90);
                        std::cout<<"<<";
                        retour-=1;
                    }
                    break;
                default:
                    break;
                }
                break;
            case 13 :
                ok=true;
                for(int u=90; u<=115; u+=2)
                {
                    pConsole->gotoLigCol(i, u);
                    std::cout<<"  >>";
                    pConsole->gotoLigCol(i,113-(u-90));
                    std::cout<<"<<  ";
                    pConsole->pause(100);
                }
                break;
            default :
                break;
            }
        }
        while(!ok);
        switch (retour)
        {
        case 1 :
            pConsole->clean();
            Puzzle::intro(pConsole);
            if (jeubase->getm_niveau()!=5)
            niveau->affichermat(pConsole, false,true,'1');
            else
                niveau->affichermat(pConsole, false,false, '1');
            Puzzle::affitimer(pConsole,*jeubase,1);
            quit=true;
            break;
        case 2:
            jeubase->sauvegarde(niveau, Snoop.getm_posx(), Snoop.getm_posy(), pConsole);
            save=true;
            break;
        case 3:
            recommencer=true;
            pConsole->clean();
            quit=true;
            break;
        case 4:
            Puzzle::menuoption(pConsole,affichage, *niveau, Snoop);
            niveau->setm_affit(affichage);
            Puzzle::intro(pConsole);
            if (jeubase->getm_niveau()!=5)
            niveau->affichermat(pConsole, false,true,'1');
            else
                niveau->affichermat(pConsole, false,false,'1');
            Puzzle::affitimer(pConsole,*jeubase,1);
            quit=true;

            break;
        case 5:
            if (save==false)
            {
                pConsole->clean();
                pConsole->gotoLigCol(30,60);
                std::cout<<"Vous allez perdre la partie si vous ne sauvegardez pas ! ";
                pConsole->gotoLigCol(25,60);
                std::cout<<" O pour sauvegarder avant de quitter          N pour quitter";
                char c;
                pConsole->gotoLigCol(34,70);
                std::cin>>c;
                if (c=='O'|| c=='o')
                    jeubase->sauvegarde(niveau, Snoop.getm_posx(), Snoop.getm_posy(), pConsole);
            }
            quit=true;
            quittergam=true;
            break;
        default:
            break;

        }
    }
    while (quit==false);
}

void Puzzle::jailafoi(Console* pConsole, bool& affichage)
{
    int taillex, tailley,x=10,y=5;
    bool ok, edit;
    std::string pseudo;
    jeubase.setNiveau(10);
    do{
    pConsole->clean();
    pConsole->gotoLigCol(30,70);
    std::cout<<"Veuillez saisir la taille de votre matrice :";
    pConsole->gotoLigCol(32,75);

    std::cout<<"X (>0 et <=30) : ";
    std::cin>>taillex;
    if (taillex<=0||taillex>30){
        std::cerr<<"Saisie incorrecte !";pConsole->pause(1500);}
    }while(taillex<=0||taillex>30);
    pConsole->gotoLigCol(34,75);
    do{
    std::cout<<"Y(>0 et <=25) : ";
    std::cin>>tailley;
    if (tailley<=0||tailley>25)
        std::cerr<<"Saisie incorrecte !";
    }while (tailley<=0||tailley>25);
    Niveau modecrea(taillex, tailley, "a", true);
    Personnage Snoop(10,5);
    edit=modecrea.editeur(pConsole, x, y, pseudo);
            if(edit == true)
            {
                Snoop.setabsoluteposx(x);
                Snoop.setabsoluteposy(y);
                ok=jeubase.chargement(&modecrea, &x, &y, pConsole, pseudo);
                if(ok == true)
                {
                    jeubase.setNiveau(8);
                    Puzzle::gam(pConsole,affichage,&modecrea, Snoop, ok);
                }
            }
            else accueil(pConsole);

}
