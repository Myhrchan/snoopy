#ifndef BALLE_H_INCLUDED
#define BALLE_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include "console.h"
#include "Personnage.h"

class Balle
{
private:
    int m_x;
    int m_y;
    int m_depx;
    int m_depy;
    void setX(int i);
    void setY(int i);
    void setDepx(bool neg); /// neg = true s'il faut inverser le déplacement
    void setDepy(bool neg); /// Idem

public:
    /// Constructeurs et destructeurs
    Balle();
    Balle(int _x, int _y, int _depx, int _depy);
    ~Balle();

    /// Méthodes
    void depballe(std::vector<std::vector<char> >& mat, Console* pConsole,Personnage& Snoop);///Déplacement de la balle
    void collision(std::vector<std::vector<char> >& mat);///Collision et rebond
};

#endif // BALLE_H_INCLUDED
