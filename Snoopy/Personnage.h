#ifndef PERSONNAGE_H_INCLUDED
#define PERSONNAGE_H_INCLUDED

#include <iostream>
#include <vector>
#include "console.h"
#include "jeu.h"

class Personnage
{
private :
    ///Attributs et m�thodes non accessibles
    int m_posx, m_posy;
    bool m_mort;
    void bougerHaut(std::vector< std::vector<char> >& matrice,Console* pConsole, Jeu& jeubase, int& tempsmenu);
    void bougerBas(std::vector< std::vector<char> >& matrice,Console* pConsole, Jeu& jeubase, int& tempsmenu);
    void bougerGauche(std::vector< std::vector<char> >& matrice,Console* pConsole, Jeu& jeubase, int& tempsmenu);
    void bougerDroite(std::vector< std::vector<char> >& matrice,Console* pConsole, Jeu& jeubase, int& tempsmenu);

public :
    ///Constructeur, Destructeur et accesseurs
    Personnage();
    ~Personnage();
    Personnage (int x, int y);
    int getm_posx()const;
    int getm_posy()const;
    bool getm_mort() const;
    void setmort(bool etat);
    void setm_posx(int a, std::vector< std::vector<char> >& matrice);
    void setm_posy(int a, std::vector< std::vector<char> >& matrice);
    void setabsoluteposx(int a);
    void setabsoluteposy(int a);

    ///M�thodes
    void mouvement(char direction,std::vector < std::vector<char> >& matrice,Console* pConsole, Jeu& jeubase, int& tempsmenu);///Mouvement
    void qqchosesepasse(std::vector< std::vector<char> >& matrice, Jeu& jeubase, int& tempsmenu);///Test cas particulier
    void testTypeBloc(std::vector<std::vector<char> >& vect, int x, int y, Console* pConsole);
    void blocc(std::vector<std::vector<char> >& vect,Console* pConsole);///Casse bloc

};

#endif // PERSONNAGE_H_INCLUDED
