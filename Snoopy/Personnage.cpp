#include "Personnage.h"

///Constructeur Personnage
Personnage::Personnage(int x, int y) : m_posx(x), m_posy(y), m_mort(false)
{

}

///Destructeur
Personnage::~Personnage()
{

}

int Personnage::getm_posx() const
{
    return m_posx;
}

int Personnage::getm_posy() const
{
    return m_posy;
}

bool Personnage::getm_mort() const
{
    return m_mort;
}

void Personnage::setmort(bool etat)
{
    m_mort=etat;
}

void Personnage::setabsoluteposx(int a)
{
    if(a>=1 && a<=20)
        m_posx=a;
}

void Personnage::setabsoluteposy(int a)
{
    if(a>=1 && a<=10)
        m_posy=a;
}

void Personnage::setm_posx(int a, std::vector< std::vector<char> >& matrice)
{
    if (matrice [m_posx+a][m_posy]!='$')
        m_posx+=a;
}

void Personnage::setm_posy(int a, std::vector< std::vector<char> >& matrice)
{
    if (matrice [m_posx][m_posy+a]!='$')
        m_posy+=a;
}

///Test de cas particuliers (sur un o, sur un b, sur un t, sur un x)
void Personnage::qqchosesepasse(std::vector< std::vector<char> >& matrice, Jeu& jeubase,int& tempsmenu)
{
    if (matrice [m_posx][m_posy]=='O')
    {
        jeubase.setm_nboiseau(1);
    }
    else if(matrice [m_posx][m_posy]=='B')
    {
        setmort(true);
    }
    else if (matrice [m_posx][m_posy]=='T')
    {
        setmort(true);
    }
    else if (matrice [m_posx][m_posy]=='X')
    {
        tempsmenu+=5;
    }
}

/// Sp de test de la pr�sence et du type de bloc
/// Re�oit le vecteur, les coordonn�es de la case � tester (x, y) et les coordonn�es de Snoopy (s_x, s_y)
/// Renvoie true si Snoopy est mort (si il a touch� un bloc pi�g�), false sinon
void Personnage::testTypeBloc(std::vector<std::vector<char> >& vect, int x, int y, Console* pConsole)
{
    if(vect[x][y] == 'P') //poussable
    {
        ///D�placement du bloc � droite
        if((x == m_posx+1) && (vect[x+1][y] != '$')&& (vect[x+1][y] != 'N')&& (vect[x+1][y] != 'C')&& (vect[x+1][y] != 'T')&& (vect[x+1][y] != 'O')&& (vect[x+1][y] != 'B'))
        {
            vect[x][y] = ' ';
            vect[x+1][y] = 'N'; // le bloc poussable devient un bloc fixe
            pConsole->gotoLigCol(19+2*y,48+4*x);
            std::cout<<" ";
            pConsole->gotoLigCol(19+2*y,48+4*(x+1));
            std::cout<<"N";
        }

        ///D�placement du bloc � gauche
        else if((x == m_posx-1) && (vect[x-1][y] != '$')&& (vect[x-1][y] != 'N')&& (vect[x-1][y] != 'C')&& (vect[x-1][y] != 'T')&& (vect[x-1][y] != 'O')&& (vect[x-1][y] != 'B'))
        {
            vect[x][y] = ' ';
            vect[x-1][y] = 'N'; // le bloc poussable devient un bloc fixe
            pConsole->gotoLigCol(19+2*y,48+4*x);
            std::cout<<" ";
            pConsole->gotoLigCol(19+2*y,48+4*(x-1));
            std::cout<<"N";

        }

        ///D�placement du bloc en haut
        else if((y == m_posy-1) && (vect[x][y-1] != '$')&& (vect[x][y-1] != 'N')&& (vect[x][y-1] != 'C')&& (vect[x][y-1] != 'T')&& (vect[x][y-1] != 'O')&& (vect[x][y-1] != 'B'))
        {
            vect[x][y] = ' ';
            vect[x][y-1] = 'N'; // le bloc poussable devient un bloc fixe
            pConsole->gotoLigCol(19+2*y,48+4*x);
            std::cout<<" ";
            pConsole->gotoLigCol(19+2*(y-1),48+4*x);
            std::cout<<"N";

        }

        ///D�placement du bloc en bas
        else if((y == m_posy+1) && (vect[x][y+1] != '$')&& (vect[x][y+1] != 'N')&& (vect[x][y+1] != 'C')&& (vect[x][y+1] != 'T')&& (vect[x][y+1] != 'O')&& (vect[x][y+1] != 'B'))
        {
            vect[x][y] = ' ';
            vect[x][y+1] = 'N'; // le bloc poussable devient un bloc fixe
            pConsole->gotoLigCol(19+2*y,48+4*x);
            std::cout<<" ";
            pConsole->gotoLigCol(19+2*(y+1),48+4*x);
            std::cout<<"N";
        }
    }
}

///casser blocs aux alentours
void Personnage::blocc(std::vector<std::vector<char> >& vect,Console* pConsole)
{
    if(vect[m_posx+1][m_posy] == 'C') //cassable
    {
        int bonus;
        vect[m_posx+1][m_posy] = ' ';
        pConsole->gotoLigCol(19+2*m_posy,48+4*(m_posx+1));
        bonus=rand()%5;
        if (bonus==2)
        {
            std::cout<<"X";
            vect[m_posx+1][m_posy] = 'X';
        }
        else
            std::cout<<" ";
    }
    else if(vect[m_posx-1][m_posy] == 'C') //cassable
    {
        int bonus;
        vect[m_posx-1][m_posy] = ' ';
        pConsole->gotoLigCol(19+2*m_posy,48+4*(m_posx-1));
        bonus=rand()%5;
        if (bonus==2)
        {
            std::cout<<"X";
            vect[m_posx-1][m_posy] = 'X';
        }
        else
            std::cout<<" ";
    }
    else if(vect[m_posx][m_posy+1] == 'C') //cassable
    {
        int bonus;
        vect[m_posx][m_posy+1] = ' ';
        pConsole->gotoLigCol(19+2*(m_posy+1),48+4*m_posx);
        bonus=rand()%5;
        if (bonus==2)
        {
            std::cout<<"X";
            vect[m_posx][m_posy+1] = 'X';
        }
        else
            std::cout<<" ";
    }
    else if(vect[m_posx][m_posy-1] == 'C') //cassable
    {
        int bonus;
        vect[m_posx][m_posy-1] = ' ';
        pConsole->gotoLigCol(19+2*(m_posy-1),48+4*m_posx);
        bonus=rand()%5;
        if (bonus==2)
        {
            std::cout<<"X";
            vect[m_posx][m_posy-1] = 'X';
        }
        else
            std::cout<<" ";
    }
}

///D�placement vers le haut
void Personnage::bougerHaut(std::vector< std::vector<char> >& matrice, Console* pConsole, Jeu& jeubase, int& tempsmenu)
{
    Personnage::setm_posy(-1, matrice);
    pConsole->gotoLigCol(19+2*(m_posy+1),48+4*(m_posx));//ancienne pos
    std::cout<<" ";
    pConsole->gotoLigCol(19+2*(m_posy),48+4*(m_posx));//Nouveau
    std::cout<<"S";
    Personnage::qqchosesepasse(matrice,jeubase,tempsmenu);
    matrice[Personnage::getm_posx()][Personnage::getm_posy()]='S';
    matrice[Personnage::getm_posx()][Personnage::getm_posy()+1]=' ';

}

///D�placement vers le bas
void Personnage::bougerBas(std::vector< std::vector<char> >& matrice, Console* pConsole, Jeu& jeubase, int& tempsmenu)
{
    setm_posy(1,matrice);
    pConsole->gotoLigCol(19+2*(m_posy-1),48+4*(m_posx));//ancienne pos
    std::cout<<" ";
    pConsole->gotoLigCol(19+2*(m_posy),48+4*(m_posx));//Nouveau
    std::cout<<"S";
    Personnage::qqchosesepasse(matrice,jeubase,tempsmenu);
    matrice[Personnage::getm_posx()][Personnage::getm_posy()]='S';
    matrice[Personnage::getm_posx()][Personnage::getm_posy()-1]=' ';

}

///D�placement vers la gauche
void Personnage::bougerGauche(std::vector< std::vector<char> >& matrice, Console* pConsole, Jeu& jeubase, int& tempsmenu)
{
    setm_posx(-1,matrice);
    pConsole->gotoLigCol(19+2*(m_posy),48+4*(m_posx+1));//ancienne pos
    std::cout<<" ";
    pConsole->gotoLigCol(19+2*(m_posy),48+4*(m_posx));//Nouveau
    std::cout<<"S";
    Personnage::qqchosesepasse(matrice,jeubase,tempsmenu);
    matrice[Personnage::getm_posx()][Personnage::getm_posy()]='S';
    matrice[Personnage::getm_posx()+1][Personnage::getm_posy()]=' ';

}

///D�placement vers la droite
void Personnage::bougerDroite(std::vector< std::vector<char> >& matrice, Console* pConsole, Jeu& jeubase, int& tempsmenu)
{
    setm_posx(1,matrice);
    pConsole->gotoLigCol(19+2*(m_posy),48+4*(m_posx-1));//ancienne pos
    std::cout<<" ";
    pConsole->gotoLigCol(19+2*(m_posy),48+4*(m_posx));//Nouveau
    std::cout<<"S";
    Personnage::qqchosesepasse(matrice,jeubase, tempsmenu);
    matrice[Personnage::getm_posx()][Personnage::getm_posy()]='S';
    matrice[Personnage::getm_posx()-1][Personnage::getm_posy()]=' ';

}

///Coordinateur de mouvement
void Personnage::mouvement(char direction,std::vector < std::vector<char> >& matrice, Console* pConsole, Jeu& jeubase, int& tempsmenu)
{
    switch (direction)
    {
    case 'z':
    case 'Z':
        if(matrice[m_posx][m_posy-1]!='N'&&matrice[m_posx][m_posy-1]!='P'&&matrice[m_posx][m_posy-1]!='C'&&matrice[m_posx][m_posy-1]!='$')
        {
            Personnage::bougerHaut(matrice,pConsole,jeubase,tempsmenu);
        }
        else if (matrice[m_posx][m_posy-1]=='P')
        {
            Personnage::testTypeBloc(matrice, m_posx,m_posy-1,pConsole);
        }
        break;

    case 's':
    case'S':
        if(matrice[m_posx][m_posy+1]!='N'&&matrice[m_posx][m_posy+1]!='P'&&matrice[m_posx][m_posy+1]!='C'&&matrice[m_posx][m_posy+1]!='$')
        {
            Personnage::bougerBas(matrice,pConsole,jeubase,tempsmenu);
        }
        else if (matrice[m_posx][m_posy+1]=='P')
        {
            Personnage::testTypeBloc(matrice, m_posx,m_posy+1,pConsole);
        }
        break;

    case 'q':
    case'Q':
        if(matrice[m_posx-1][m_posy]!='N'&&matrice[m_posx-1][m_posy]!='P'&&matrice[m_posx-1][m_posy]!='C'&&matrice[m_posx-1][m_posy]!='$')
        {
            Personnage::bougerGauche(matrice,pConsole,jeubase,tempsmenu);
        }
        else if(matrice[m_posx-1][m_posy]=='P')
        {
            Personnage::testTypeBloc(matrice, m_posx-1,m_posy,pConsole);
        }
        break;

    case'd':
    case'D':
        if(matrice[m_posx+1][m_posy]!='N'&&matrice[m_posx+1][m_posy]!='P'&&matrice[m_posx+1][m_posy]!='C'&&matrice[m_posx+1][m_posy]!='$')
        {
            Personnage::bougerDroite(matrice,pConsole,jeubase,tempsmenu);
        }
        else if(matrice[m_posx+1][m_posy]=='P')
        {
            Personnage::testTypeBloc(matrice, m_posx+1,m_posy,pConsole);
        }
        break;

    case'a':
    case'A':///Casser bloc
        Personnage::blocc(matrice,pConsole);

    default:
        break;
    }
}
