#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include "console.h"

class Niveau
{
private :///Attributs
    std::vector< std::vector<char> > m_matrice;
    int m_long, m_larg;
    int m_oiseau[8];
    std::vector <int> m_posn, m_posp,m_posc,m_post;
    std::string m_mdp;
    bool m_affit;

public :
    ///Constructeur, destructeur et accesseurs
    Niveau();
    Niveau(int lon, int larg, std::string motpasse, bool affit);
    ~Niveau();
    int getm_long() const;
    int getm_larg() const;
    char getLettre(int i, int j) const;
    bool getm_affit()const;
    void setm_affit(bool affic);
    std::vector<  std::vector<char> >& getm_matrice();
    void setPosOiseau( int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4);
    void setPosblocn( int x, int y);
    void setPosblocp( int x, int y);
    void setPosblocc( int x, int y);
    void setPosbloct( int x, int y);
    void setCaseMat(int x, int y, char lettre);

    ///M�thodes
    void creermatrice();///Cr�ation de la matrice
    void affichermat(Console* pConsole, bool edit, bool colo,char option, int stat=0);///Affichage de la matrice
    void nettoyerVect();///Rempli le vecteur de 0
    void creerNiveau(int niveau);///Cr�ation d'un niveau
    bool editeur(Console* pConsole, int& posx, int& posy, std::string& nom);///Mode �diteur
};

#endif // NIVEAU_H_INCLUDED
