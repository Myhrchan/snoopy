#ifndef PUZZLE_H_INCLUDED
#define PUZZLE_H_INCLUDED


#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <ctime>
#include "balle.h"
#include "console.h"
#include "jeu.h"
#include "Niveau.h"
#include "Personnage.h"

class Puzzle
{
    Jeu jeubase;

public:
    int menuprinc(Console* pConsole);///Affichage du menu principal
    void menuoption(Console* pConsole,bool& affichage, Niveau& niv, Personnage& Snoop);///Affichage du menu option
    void lancjeu();///Lancement du jeu
    void gam(Console* pConsole,bool affichage, Niveau* niv, Personnage& Snoop, bool ok);///Boucle de jeu
    void accueil(Console* cons);///Ecran d'accueil
    void intro(Console* pConsole);///Intro pour le jeu
    void affitimer(Console* pConsole, Jeu jeubase, int colo);///Afficher temps restant
    void progtimer(clock_t timeini, Console* pConsole, int tempsmenu);///Affichage de la progression du timer
    void menuechap(bool& quittergam, Console* pConsole, Jeu* jeubase, Niveau* niveau, Personnage& Snoop, bool& affichage, bool& recommencer);///Affichage du menu �chap
    void jailafoi(Console* pConsole, bool& affichage);///Mode "j'ai la foi"
};

#endif // PUZZLE_H_INCLUDED
