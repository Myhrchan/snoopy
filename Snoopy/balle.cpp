#include "balle.h"

/// Constructeurs de Balle
Balle::Balle()
    : m_x(1), m_y(1), m_depx(1), m_depy(1)
{
}

Balle::Balle(int _x, int _y, int _depx, int _depy)
    : m_x(_x), m_y(_y), m_depx(_depx), m_depy(_depy)
{
}

///Destructeur de Balle
Balle::~Balle()
{
}

void Balle::setX(int i)
{
    if(i==1 || i==-1) m_x = m_x + i;
}

void Balle::setY(int i)
{
    if(i==1 || i==-1) m_y = m_y + i;
}

void Balle::setDepx(bool neg)
{
    if (neg == true) m_depx = -m_depx;
}

void Balle::setDepy(bool neg)
{
    if (neg == true) m_depy = -m_depy;
}

/// Incr�mentation des coordonn�es de la balle selon son d�placement et changement du deplacement si la balle rencontre un obstacle
void Balle::depballe(std::vector<std::vector<char> >& mat, Console* pConsole, Personnage& Snoop)
{
    setX(m_depx);
    setY(m_depy);
    if (mat[m_x][m_y]=='S')
        Snoop.setmort(true);
    mat[m_x-m_depx][m_y-m_depy]=' ';
    mat[m_x][m_y]='B';
    pConsole->gotoLigCol(19+2*(m_y-m_depy),48+4*(m_x-m_depx));
    std::cout<<" ";
    pConsole->gotoLigCol(19+2*m_y,48+4*m_x);
    pConsole->setColor(COLOR_RED);
    std::cout<<"B";
    pConsole->setColor(COLOR_WHITE);

}
///Sous-programme g�rant la collision de la balle et son rebond
void Balle::collision (std::vector<std::vector<char> >& mat)
{

    // Rebondir � droite vers le bas
    if((m_depx > 0) && (m_depy > 0) && ((mat[m_x + 1][m_y] == 'N') || (mat[m_x + 1][m_y] == 'T')|| (mat[m_x + 1][m_y] == 'O') || (mat[m_x + 1][m_y] == 'P') || (mat[m_x + 1][m_y] == 'C') || (mat[m_x + 1][m_y] == '$')))
    {

        setDepx(true);
    }
    // Rebondir � droite ver le haut
    if((m_depx > 0) && (m_depy < 0) && ((mat[m_x + 1][m_y] == 'N') || (mat[m_x + 1][m_y] == 'T')|| (mat[m_x + 1][m_y] == 'O') || (mat[m_x + 1][m_y] == 'P') || (mat[m_x + 1][m_y] == 'C') || (mat[m_x + 1][m_y] == '$')))
    {
        setDepx(true);
    }
    // Rebondir � gauche vers le bas
    if((m_depx < 0) && (m_depy > 0) && ((mat[m_x - 1][m_y] == 'N') || (mat[m_x - 1][m_y] == 'T')|| (mat[m_x - 1][m_y] == 'O') || (mat[m_x - 1][m_y] == 'P') || (mat[m_x - 1][m_y] == 'C') || (mat[m_x - 1][m_y] == '$')))
    {
        setDepx(true);
    }
    // Rebondir � gauche vers le haut
    if((m_depx < 0) && (m_depy < 0) && ((mat[m_x - 1][m_y] == 'N') || (mat[m_x - 1][m_y] == 'T')|| (mat[m_x - 1][m_y] == 'O') || (mat[m_x - 1][m_y] == 'P') || (mat[m_x - 1][m_y] == 'C') || (mat[m_x - 1][m_y] == '$')))
    {
        setDepx(true);
    }

    // Rebondir en haut vers la droite
    if((m_depx > 0) && (m_depy < 0) && ((mat[m_x][m_y - 1] == 'N') || (mat[m_x][m_y - 1] == 'T')|| (mat[m_x][m_y - 1] == 'O') || (mat[m_x][m_y - 1] == 'P') || (mat[m_x][m_y - 1] == 'C') || (mat[m_x][m_y - 1] == '$')))
    {
        setDepy(true);
    }
    // Rebondir en haut vers la gauche
    if((m_depx < 0) && (m_depy < 0) && ((mat[m_x][m_y - 1] == 'N') || (mat[m_x][m_y - 1] == 'T')|| (mat[m_x][m_y - 1] == 'O') || (mat[m_x][m_y - 1] == 'P') || (mat[m_x][m_y - 1] == 'C') || (mat[m_x][m_y - 1] == '$')))
    {
        setDepy(true);
    }
    // Rebondir en bas vers la droite
    if((m_depx > 0) && (m_depy > 0) && ((mat[m_x][m_y + 1] == 'N') || (mat[m_x][m_y + 1] == 'T')|| (mat[m_x][m_y + 1] == 'O') || (mat[m_x][m_y + 1] == 'P') || (mat[m_x][m_y + 1] == 'C') || (mat[m_x][m_y + 1] == '$')))
    {
        setDepy(true);
    }
    // Rebondir en bas vers la gauche
    if((m_depx < 0) && (m_depy > 0) && ((mat[m_x][m_y + 1] == 'N') || (mat[m_x][m_y + 1] == 'T')|| (mat[m_x][m_y + 1] == 'O') || (mat[m_x][m_y + 1] == 'P') || (mat[m_x][m_y + 1] == 'C') || (mat[m_x][m_y + 1] == '$')))
    {
        setDepy(true);
    }

    /// Obstacles en diagonale
    // Diagonale vers le bas � droite
    if((m_depx > 0) && (m_depy > 0) && ((mat[m_x + 1][m_y + 1] == 'N') || (mat[m_x + 1][m_y + 1] == 'T')|| (mat[m_x + 1][m_y + 1] == 'O') || (mat[m_x + 1][m_y + 1] == 'P') || (mat[m_x + 1][m_y + 1] == 'C') || (mat[m_x + 1][m_y + 1] == '$')))
    {
        setDepx(true);
        setDepy(true);
        // Rebondir � gauche vers le haut
        if((m_depx < 0) && (m_depy < 0) && ((mat[m_x - 1][m_y] == 'N') || (mat[m_x + 1][m_y] == 'T')|| (mat[m_x - 1][m_y] == 'O') || (mat[m_x - 1][m_y] == 'P') || (mat[m_x - 1][m_y] == 'C') || (mat[m_x - 1][m_y] == '$')))
        {
            setDepx(true);
        }
        // Rebondir en haut vers la gauche
        if((m_depx < 0) && (m_depy < 0) && ((mat[m_x][m_y - 1] == 'N') || (mat[m_x][m_y - 1] == 'T')|| (mat[m_x][m_y - 1] == 'O') || (mat[m_x][m_y - 1] == 'P') || (mat[m_x][m_y - 1] == 'C') || (mat[m_x][m_y - 1] == '$')))
        {
            setDepy(true);
        }
    }

    // Diagonale vers le haut � droite
    if((m_depx > 0) && (m_depy < 0) && ((mat[m_x + 1][m_y - 1] == 'N') || (mat[m_x + 1][m_y - 1] == 'T')|| (mat[m_x + 1][m_y - 1] == 'O') || (mat[m_x + 1][m_y - 1] == 'P') || (mat[m_x + 1][m_y - 1] == 'C') || (mat[m_x + 1][m_y - 1] == '$')))
    {
        setDepx(true);
        setDepy(true);
        // Rebondir � gauche vers le bas
        if((m_depx < 0) && (m_depy > 0) && ((mat[m_x - 1][m_y] == 'N') || (mat[m_x - 1][m_y] == 'T')|| (mat[m_x - 1][m_y] == 'O') || (mat[m_x - 1][m_y] == 'P') || (mat[m_x - 1][m_y] == 'C') || (mat[m_x - 1][m_y] == '$')))
        {
            setDepx(true);
        }
        // Rebondir en bas vers la gauche
        if((m_depx < 0) && (m_depy > 0) && ((mat[m_x][m_y + 1] == 'N') || (mat[m_x][m_y + 1] == 'T')|| (mat[m_x][m_y + 1] == 'O') || (mat[m_x][m_y + 1] == 'P') || (mat[m_x][m_y + 1] == 'C') || (mat[m_x][m_y + 1] == '$')))
        {
            setDepy(true);
        }
    }

    // Diagonale vers le bas � gauche
    if((m_depx < 0) && (m_depy > 0) && ((mat[m_x - 1][m_y + 1] == 'N') || (mat[m_x - 1][m_y + 1] == 'T')|| (mat[m_x - 1][m_y + 1] == 'O') || (mat[m_x - 1][m_y + 1] == 'P') || (mat[m_x - 1][m_y + 1] == 'C') || (mat[m_x - 1][m_y + 1] == '$')))
    {
        setDepx(true);
        setDepy(true);
        // Rebondir � droite vers le haut
        if((m_depx > 0) && (m_depy < 0) && ((mat[m_x + 1][m_y] == 'N') || (mat[m_x + 1][m_y] == 'T')|| (mat[m_x + 1][m_y] == 'O') || (mat[m_x + 1][m_y] == 'P') || (mat[m_x + 1][m_y] == 'C') || (mat[m_x + 1][m_y] == '$')))
        {
            setDepx(true);
        }
        // Rebondir en haut vers la droite
        if((m_depx > 0) && (m_depy < 0) && ((mat[m_x][m_y - 1] == 'N') || (mat[m_x][m_y - 1] == 'T')|| (mat[m_x][m_y - 1] == 'O') || (mat[m_x][m_y - 1] == 'P') || (mat[m_x][m_y - 1] == 'C') || (mat[m_x][m_y - 1] == '$')))
        {
            setDepy(true);
        }
    }

    // Diagonale vers le haut � gauche
    if((m_depx < 0) && (m_depy < 0) && ((mat[m_x - 1][m_y - 1] == 'N') || (mat[m_x - 1][m_y - 1] == 'T')|| (mat[m_x - 1][m_y - 1] == 'O') || (mat[m_x - 1][m_y - 1] == 'P') || (mat[m_x - 1][m_y - 1] == 'C') || (mat[m_x - 1][m_y - 1] == '$')))
    {
        setDepx(true);
        setDepy(true);
        // Rebondir � droite vers le bas
        if((m_depx > 0) && (m_depy > 0) && ((mat[m_x + 1][m_y] == 'N') || (mat[m_x + 1][m_y] == 'T')|| (mat[m_x + 1][m_y] == 'O') || (mat[m_x + 1][m_y] == 'P') || (mat[m_x + 1][m_y] == 'C') || (mat[m_x + 1][m_y] == '$')))
        {
            setDepx(true);
        }
        // Rebondir en bas vers la droite
        if((m_depx > 0) && (m_depy > 0) && ((mat[m_x][m_y + 1] == 'N') || (mat[m_x][m_y + 1] == 'T')|| (mat[m_x][m_y + 1] == 'O') || (mat[m_x][m_y + 1] == 'P') || (mat[m_x][m_y + 1] == 'C') || (mat[m_x][m_y + 1] == '$')))
        {
            setDepy(true);
        }
    }
}
